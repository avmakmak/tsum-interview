package tsum.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


/**
 * @project testProject
 * Created by Andrey M on 01.01.2021.
 */
@DefaultUrl("page:authorization.page")
public class AuthorizationPage extends PageObject {
	@FindBy(xpath = "*//auth-layout//input[contains(@class,'login-input')]")
	private WebElementFacade loginInput;

	@FindBy(xpath = "*//auth-layout//input[contains(@class,'pwd-input')]")
	private WebElementFacade passwordInput;

	@FindBy(xpath = "*//auth-layout//button[@type='submit']")
	private WebElementFacade submitLoginButton;

	public void inputEmailField(String text) {
		loginInput.waitUntilVisible();
		enter(text).into(loginInput);
	}

	public void inputPasswordField(String text) {
		passwordInput.waitUntilVisible();
		enter(text).into(passwordInput);
	}

	public void submitButtonClick() {
		passwordInput.waitUntilEnabled();
		submitLoginButton.click();
	}
}
