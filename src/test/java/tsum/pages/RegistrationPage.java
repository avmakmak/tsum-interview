package tsum.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


/**
 * @project testProject
 * Created by Andrey M on 01.01.2021.
 */
@DefaultUrl("page:registration.page")
public class RegistrationPage extends PageObject {
	@FindBy(xpath = "*//auth-register//input[@formcontrolname='email']")
	private WebElementFacade loginInput;

	@FindBy(xpath = "*//auth-register//input[@type='password']")
	private WebElementFacade passwordInput;

	@FindBy(xpath = "*//auth-register//button[@type='submit']")
	private WebElementFacade submitRegistrationButton;

	public void inputEmailField(String text) {
		loginInput.waitUntilVisible();
		enter(text).into(loginInput);
	}

	public void inputPasswordField(String text) {
		passwordInput.waitUntilVisible();
		enter(text).into(passwordInput);
	}

	public void submitButtonClick() {
		passwordInput.waitUntilEnabled();
		submitRegistrationButton.click();
	}

	public boolean submitRegistrationButtonIsVisible() {
		return submitRegistrationButton.isVisible();
	}
}
